import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { IAuth } from "../../views/sessions/auth";

@Injectable()
export class AuthUserService {

    constructor(private apiClient: HttpClient) {
    }

    public SignIn(auth: IAuth): Observable<any> {
        let credentials = 'username=' + auth.userName + '&password=' + auth.password + '&grant_type=password';
        let reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True', 'Access-Control-Allow-Origi': '* always' });
        return this.apiClient.post<any>('http://localhost:53696/' + 'token', encodeURI(credentials), { headers: reqHeader });
    }

    public register(auth: IAuth): Observable<any> {
        const url = `http://localhost:53696/api/users/register`;
        let reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True', 'Access-Control-Allow-Origi': '* always' });
        return this.apiClient.post<any>(url, auth, { headers: reqHeader });
    }
}
