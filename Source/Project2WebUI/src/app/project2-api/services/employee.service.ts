import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IEmployee } from '../models';

@Injectable()
export class EmployeeService {
    constructor(private _apiClient: HttpClient) { }

    private resolveUrl(employeeId?: number): string {
        const url = `http://localhost:53696/api/employees`;
        if (employeeId) {
            return `${url}/${employeeId}`;
        }
        return url;
    }

    // Get all employees
    public getAllEmployees(): Observable<IEmployee[]> {
        const url = this.resolveUrl();
        return this._apiClient.get<IEmployee[]>(url);
    }
}
