import { NgModule } from '@angular/core';
import { EmployeeService } from './services';
import { AuthUserService } from './services/auth-user.service';
@NgModule({
    imports: [
    ],
    exports: [],
    providers: [
        AuthUserService,
        EmployeeService
    ]
})
export class Project2ApiModule {

}
