import { Signup2Component } from './signup2/signup2.component';
import { Routes } from "@angular/router";
import { Signin2Component } from './signin2/signin2.component';

export const SessionsRoutes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: 'signin2', pathMatch: 'full' },
      {
        path: "signup2",
        component: Signup2Component,
        data: { title: "Signup2" }
      },
      {
        path: "signin2",
        component: Signin2Component,
        data: { title: "Signin2" }
      }
    ]
  }
];
