import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { SharedMaterialModule } from 'app/shared/shared-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SessionsRoutes } from "./sessions.routing";
import { Signup2Component } from './signup2/signup2.component';
import { Signin2Component } from './signin2/signin2.component';
import { AuthUserService } from '../../project2-api/services/auth-user.service';
import { HttpClientModule } from '@angular/common/http';
import { EmployeeService } from 'app/project2-api/services/employee.service';
import { Project2ApiModule } from 'app/project2-api/project2-api.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    Project2ApiModule,
    ReactiveFormsModule,
    SharedMaterialModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    RouterModule.forChild(SessionsRoutes)
  ],
  declarations: [Signup2Component,
    Signin2Component,
  ],
  providers: [AuthUserService,
    EmployeeService,
    // Authenticate User
    // AuthGuard,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: AuthInterceptor,
    //   multi: true
    // }
  ]
})
export class SessionsModule { }