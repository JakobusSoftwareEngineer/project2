import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthUserService } from 'app/project2-api/services/auth-user.service';
import { EmployeeService } from 'app/project2-api/services/employee.service';
import { CustomValidators } from 'ng2-validation';
import { IAuth } from '../auth';

@Component({
  selector: 'app-signin2',
  templateUrl: './signin2.component.html',
  styleUrls: ['./signin2.component.scss']
})
export class Signin2Component implements OnInit {

  signinForm: FormGroup;
  public credentials = <IAuth>{};

  constructor(private fb: FormBuilder,
    private authUserService: AuthUserService,
    private employeeService: EmployeeService) { }

  ngOnInit() {

    const password = new FormControl('', Validators.required);
    const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

    this.signinForm = this.fb.group(
      {
        email: ["", [Validators.required, Validators.email]],
        password: password,
        agreed: [false, Validators.required]
      }
    );
  }

  onSubmit() {
    // test employee
    this.employeeService.getAllEmployees().subscribe((result) => {
      if (result) {
        const t = result;
      }
    }, error => {
      const er = error;
    });


    // if (!this.signinForm.invalid) {
    //   // do what you wnat with your data
    //   const signupData = this.signinForm.value;
    //   this.credentials.userName = signupData.email;
    //   this.credentials.password = signupData.password;
    //   this.authUserService.SignIn(this.credentials).subscribe((result) => {
    //     if (result) {
    //       const t = null;
    //     }
    //   }, error => {
    //     const err = error;
    //   });


    //   console.log(this.signinForm.value);
    // }
  }
}
