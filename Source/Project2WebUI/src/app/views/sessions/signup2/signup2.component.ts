import { CustomValidators } from 'ng2-validation';
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IAuth } from '../auth';
import { AuthUserService } from 'app/project2-api/services/auth-user.service';

@Component({
  selector: "app-signup2",
  templateUrl: "./signup2.component.html",
  styleUrls: ["./signup2.component.scss"]
})
export class Signup2Component implements OnInit {
  signupForm: FormGroup;
  private logingin: Subscription;
  public credentials = <IAuth>{};

  constructor(private fb: FormBuilder,
    private authService: AuthUserService,
    private router: Router) { }

  ngOnInit() {

    const password = new FormControl('', Validators.required);
    const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

    this.signupForm = this.fb.group(
      {
        firstName: ["", Validators.required],
        lastName: ["", Validators.required],
        username: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        password: password,
        confirmPassword: confirmPassword,
        agreed: [false, Validators.required]
      }
    );
  }

  onSubmit() {
    const signupData = this.signupForm.value;
    if (!this.signupForm.invalid) {
      // do what you wnat with your data
      this.credentials.userName = signupData.username;
      this.credentials.password = signupData.password;
      this.logingin = this.authService.register(this.credentials).subscribe((result) => {
        if (result) {
          this.router.navigate(['/home']);
        }
      });
      //    error => {
      //     const err = error;
      //   });
      // }
    }
  }
}