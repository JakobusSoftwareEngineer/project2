﻿using CarAuctionPlatform_API.Domain;
using Project2.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Domain
{
    public class UserAuthDomainService : PasswordHasher
    {
        // Login
        public async Task<bool> LoginAsync(string username, string password)
        {

            using (var db = this.CreateContext())
            {
                var query = from member in db.AuthUsers
                            where member.UserName == username
                            select member;
                var entity = await query.FirstOrDefaultAsync();

                if (entity == null)
                    return false;

                if (!VerifyPasswordHash(password, entity.PasswordHash, entity.PasswordSalt))
                    return false;

                return true;
            };

        }

        // Verify password 
        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        // Register a user
        public async Task<AuthUser> RegsisterAsync(Auth model)
        {
            using (var db = this.CreateContext())
            {
                byte[] passwordHash, passwordSalt;

                var query = from member in db.AuthUsers
                            where member.UserName == model.UserName
                            select member;
                var entity = await query.FirstOrDefaultAsync();
                if (entity != null)
                    return null;
                else
                {
                    CreatePasswordHash(model.Password, out passwordHash, out passwordSalt);

                    AuthUser user = new AuthUser
                    {
                        UserName = model.UserName,
                        PasswordHash = passwordHash,
                        PasswordSalt = passwordSalt,
                    };
                    await CreateNewUserAsync(user);
                    return user;
                }

            };

        }

        // Create Password Hash
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        //Create new User in db
        public async Task CreateNewUserAsync(AuthUser user)
        {
            using (var db = this.CreateContext())
            {
                var entity = user.ToEntity();
                Guid g = Guid.NewGuid();
                entity.Id = g;
                db.AuthUsers.Add(entity);
                await db.SaveChangesAsync();
                user.Id = entity.Id;
            }
        }

        // Change user password
        public async Task UpdateUserPasswordAsync(AuthUser user, string newPassword)
        {
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(newPassword, out passwordHash, out passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            using (var db = this.CreateContext())
            {
                var entity = user.ToEntity();
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }

        // Get User by username
        public async Task<AuthUser> GetUserByUsernameAsync(string username)
        {
            using (var db = this.CreateContext())
            {
                //var query = from user in db.AuthUsers
                //            where user.UserName == username
                //            select user;
                //var entity = await query.FirstOrDefaultAsync();
                //var model = entity.FromEntity(entity);
                //model.PasswordHash = null;
                //model.PasswordSalt = null;
                return null;
            };
        }
    }
}
