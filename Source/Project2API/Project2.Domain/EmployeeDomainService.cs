﻿using CarAuctionPlatform_API.Domain;
using Project2.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Domain
{
    public class EmployeeDomainService : DomainServiceBase
    {
        // Get all Employees
        public async Task<IEnumerable<Employee>> GetEmployeesAsync()
        {
            using (var db = this.CreateContext())
            {
                var query = from employee in db.Employees
                            select employee;
                var entities = await query.ToListAsync();
                var models = entities.Select(c => Employee.FromEntity(c));
                return models;
            }
        }
    }
}
