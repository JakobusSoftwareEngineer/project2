﻿using Project2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarAuctionPlatform_API.Domain
{
    public class DomainServiceBase
    {
        protected Project2DbContext CreateContext()
        {
            Project2DbContext dbContext = new Project2DbContext();
            return dbContext;
        }
    }
}
