﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Domain.Models
{
    public class AuthUser
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }


        public Project2.Model.AuthUser ToEntity()
        {
            return new Project2.Model.AuthUser()
            {
                Id = this.Id,
                UserName = this.UserName,
                PasswordHash = this.PasswordHash,
                PasswordSalt = this.PasswordSalt,

            };
        }

        public static AuthUser FromEntity(Project2.Model.AuthUser entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new AuthUser()
            {
                Id = entity.Id,
                UserName = entity.UserName,
                PasswordHash = entity.PasswordHash,
                PasswordSalt = entity.PasswordSalt,
            };
        }
    }
}
