﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Domain.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Number { get; set; }

        public Project2.Model.Employee ToEntity()
        {
            return new Project2.Model.Employee()
            {
                Id = this.Id,
                Number = this.Number
            };
        }

        public static Employee FromEntity(Project2.Model.Employee entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new Employee()
            {
                Id = entity.Id,
                Number = entity.Number
            };
        }
    }
}
