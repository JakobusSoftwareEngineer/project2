﻿using CarAuctionPlatform_API.API.Models;
using CarAuctionPlatform_API.Domain;
using Project2.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace CarAuctionPlatform_API.API.Controllers
{

    /// <summary>
    /// Users
    /// </summary>
    [RoutePrefix("api/users")]
    public class UserAuthController : ApiController
    {
        /// <summary>
        /// Register a user
        /// </summary>
        /// <param name="username">UserDto to create</param>
        /// <param name="password">password to create</param>
        /// <returns></returns>

        [HttpPost]
        [Route("userName/{username}/password/{password}")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> RegisterUser(AuthDto authDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var model = authDto.ToDomain();
            UserAuthDomainService domainService = new UserAuthDomainService();
            await domainService.RegsisterAsync(model);

            return Ok(true);
        }

        /// <summary>
        /// Reset user password
        /// </summary>
        /// <param name="userDto">userDto to update</param>
        /// <param name="password">password of user to update</param>
        /// <returns></returns>
        //[HttpPut]
        //[Route("resetPassword/{password}")]
        //[ResponseType(typeof(bool))]
        //public async Task<IHttpActionResult> ResetUserPassword(AuthUserDto userDto, string password)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    UserAuthDomainService domainService = new UserAuthDomainService();
        //    var model = userDto.ToDomain();
        //    await domainService.UpdateUserPasswordAsync(model, password);

        //    return Ok(true);
        //}
    }
}
