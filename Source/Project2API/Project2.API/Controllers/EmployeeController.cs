﻿using Project2.API.Models;
using Project2.Domain;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Project2.API.Controllers
{

    /// <summary>
    /// Employees
    /// </summary>
    [RoutePrefix("api/employees")]
    public class EmployeeController : ApiController
    {
        /// <summary>
        /// Get all employees
        /// </summary>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(EmployeeDto))]
        public async Task<IHttpActionResult> GetEmployees()
        {
            EmployeeDomainService domainService = new EmployeeDomainService();
            var models = await domainService.GetEmployeesAsync();
            var dtos = models.Select(c => EmployeeDto.FromDomain(c));
            return Ok(dtos);
        }
    }
}
