﻿using Microsoft.Owin.Security.OAuth;
using Project2.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Project2.API.Auth
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
           context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UserAuthDomainService domainService = new UserAuthDomainService();
            var user = await domainService.LoginAsync(context.UserName, context.Password);
            if (user == false)
            {
                context.SetError("invalid_grant", "Provided username or password is incorrect");
                return;
            }
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            context.Validated(identity);
            
        }

    }
}