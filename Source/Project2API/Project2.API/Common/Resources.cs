﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockManagement.API.Common
{
    public static class Resources
    {
        public const string Admin = "admin";
        public const string User = "user";
        public const string Customer = "customer";
        public const string Employee = "employee";
        public const string Supplier = "supplier";
        public const string Product = "product";
        public const string StockItem = "stockItem";
        public const string UserManagement = "userManagement";
        public const string Service = "service";
        public const string MyAccount = "myAccount";
        public const string EmployeeTask = "employeeTask";
        public const string EmployeeTaskType = "employeeTaskType";
        public const string TenantProfile = "tenantProfile";
        public const string TenantManagement = "tenantManagement";
        public const string TenantUserManagement = "tenantUserManagement";
        public const string StockCheckout = "stockCheckout";
    }
}