﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockManagement.Api.Common
{
    public static class Config
    {
        public static TimeSpan SessionLength
        {
            get
            {
                return TimeSpan.FromSeconds(double.Parse(ConfigurationManager.AppSettings["Security:Session:Length"]));
            }
        }

        public static TimeSpan SessionRefreshInterval
        {
            get
            {
                return TimeSpan.FromSeconds(double.Parse(ConfigurationManager.AppSettings["Security:Session:RefreshInterval"]));
            }
        }

        public static TimeSpan SessionValidateInterval
        {
            get
            {
                return TimeSpan.FromSeconds(double.Parse(ConfigurationManager.AppSettings["Security:Session:ValidateInterval"]));
            }
        }

        public static int SessionWarnOnSecondsRemaining
        {
            get
            {
                return Int32.Parse(ConfigurationManager.AppSettings["Security:Session:WarnOnSecondsRemaining"]);
            }
        }

        public static int SessionWarnInterval
        {
            get
            {
                return Int32.Parse(ConfigurationManager.AppSettings["Security:Session:WarnInterval"]);
            }
        }
    }
}