﻿using StockManagement.Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace StockManagement.Api.Common
{
    public class MyNumberValidator : ValidationAttribute
    {
        public int Minimum1 { get; set; }
        public int Maximum1 { get; set; }
        public int Minimum2 { get; set; }
        public int Maximum2 { get; set; }

        public MyNumberValidator()
        {
            this.Minimum1 = 1;
            this.Maximum1 = 10;
            this.Minimum2 = 100;
            this.Maximum2 = 200;
        }

        public override bool IsValid(object value)
        {
            string strValue = value as string;
            if (!string.IsNullOrEmpty(strValue))
            {
                int number =  int.Parse(strValue);
                return (number >= this.Minimum1 && number<= this.Maximum1) || (number >= this.Minimum2 && number <= this.Maximum2);
            }
            return true;
        }
    }

}