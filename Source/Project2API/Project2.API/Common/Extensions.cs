﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace StockManagement.API.Common
{
    public static class Extensions
    {
        public static string GetBaseUrl(this ApiController controller)
        {
            var leftPart = controller.Request.RequestUri.GetLeftPart(UriPartial.Authority);
            var rootPath = controller.RequestContext.VirtualPathRoot;
            if (rootPath.Length > 1)
                rootPath = rootPath + "/";
            var baseUrl = $"{leftPart}{rootPath}Content/FileStore/";
            return baseUrl;
        }
    }
}