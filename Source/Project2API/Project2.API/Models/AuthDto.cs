﻿using Project2.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarAuctionPlatform_API.API.Models
{
    public class AuthDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public Auth ToDomain()
        {
            return new Auth
            {
                UserName = this.UserName,
                Password = this.Password
            };
        }
    }
}