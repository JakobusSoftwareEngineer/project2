﻿using Project2.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project2.API.Models
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        public string Number { get; set; }

        public static EmployeeDto FromDomain(Employee domain)
        {
            if (domain == null)
            {
                return null;
            }
            return new EmployeeDto
            {
                Id = domain.Id,
                Number = domain.Number
            };
        }

        public Employee ToDomain()
        {
            return new Employee
            {
                Id = this.Id,
                Number = this.Number
            };
        }
    }
}